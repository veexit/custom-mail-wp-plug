<?php
/*
Plugin Name: vxtMailer
Description: Custom text for wordpress mail
Version: 1.0
Author: Vasily K aka veeXit
Author URI: http://vasily.pw/
*/

// create custom plugin settings menu
add_action('admin_menu', 'vxt_create_menu');

function vxt_create_menu() {

	//create new top-level menu
	add_menu_page('vxtMailer setting', 'vxtMailer', 'administrator', __FILE__, 'vxt_settings_page',plugins_url('/images/icon.png', __FILE__));

	//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
}


function register_mysettings() {
	//register our settings
	register_setting( 'vxt-settings-group', 'new_registraton' );
	register_setting( 'vxt-settings-group', 'a_user_name' );
	register_setting( 'vxt-settings-group', 'a_user_email' );
	register_setting( 'vxt-settings-group', 'u_hello_message' );
	register_setting( 'vxt-settings-group', 'u_welcome_message' );
	register_setting( 'vxt-settings-group', 'u_user_data' );
	register_setting( 'vxt-settings-group', 'u_user_name' );
	register_setting( 'vxt-settings-group', 'u_user_passwd' );
}

function vxt_settings_page() {
?>
<div class="wrap">
<h2>vxtMailer - Custom user text for WordPress email</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'vxt-settings-group' ); ?>
    <h3>Administrator notification text</h3>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">New user</th>
        <td><input type="text" name="new_registraton" value="<?php echo get_option('new_registraton'); ?>" placeholder="New user registration on your blog" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">User name</th>
        <td><input type="text" name="a_user_name" value="<?php echo get_option('a_user_name'); ?>" placeholder="User name" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">User email</th>
        <td><input type="text" name="a_user_email" value="<?php echo get_option('a_user_email'); ?>" placeholder="Email name"/></td>
        </tr>
    </table>
    
     <h3>User notification text</h3>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Hello message</th>
        <td><input type="text" name="u_hello_message" value="<?php echo get_option('u_hello_message'); ?>" placeholder="Hello!" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Welcome message</th>
        <td><input type="text" name="u_welcome_message" value="<?php echo get_option('u_welcome_message'); ?>" placeholder="Welcome to my site!" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">User login data</th>
        <td><input type="text" name="u_user_data" value="<?php echo get_option('u_user_data'); ?>" placeholder="Your login and username: "/></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Username</th>
        <td><input type="text" name="u_user_name" value="<?php echo get_option('u_user_name'); ?>" placeholder="Username: "/></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Password</th>
        <td><input type="text" name="u_user_passwd" value="<?php echo get_option('u_user_passwd'); ?>" placeholder="Password: "/></td>
        </tr>
    </table>
    
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>

</form>
</div>
<?php } 


if ( !function_exists('wp_new_user_notification') ) {
    function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
        $user = new WP_User($user_id);

        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);

        $message  = sprintf(__( get_option('new_registraton') . '%s:'), get_option('blogname')) . "\r\n\r\n";
        $message .= sprintf(__( get_option('a_user_name') . ': %s'), $user_login) . "\r\n\r\n";
        $message .= sprintf(__( get_option('a_user_email') . ': %s'), $user_email) . "\r\n";

        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);

        if ( empty($plaintext_pass) )
            return;

        $message  = __(get_option('u_hello_message')) . "\r\n\r\n";
        $message .= sprintf(__(get_option('u_welcome_message') . get_option('u_user_data')), get_option('blogname')) . "\r\n\r\n";
        $message .= wp_login_url() . "\r\n";
        $message .= sprintf(__(get_option('u_user_name') . ' %s'), $user_login) . "\r\n";
        $message .= sprintf(__(get_option('u_user_passwd') . ' %s'), $plaintext_pass) . "\r\n\r\n";

        wp_mail($user_email, sprintf(__('[%s] Your username and password'), get_option('blogname')), $message);

    }
}

?>
